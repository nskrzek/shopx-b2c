export default async (context) => {
  const user = await context.store.dispatch({ type: 'user/GET_USER' });
  if (!user || !user.data || !user.token || !user.data.role !== 'user') {
    await context.store.dispatch({ type: 'user/CLEAR' });
    context.redirect(401, `/account?redirect=${context.route.path}`);
  }
};
