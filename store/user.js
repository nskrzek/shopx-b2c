const lsKey = 'shopx-b2c-user';

export const state = () => ({
  data: undefined,
  token: undefined,
});

export const mutations = {
  set(s, data) {
    s.data = data.data;
    s.token = data.token;
    localStorage.setItem(lsKey, JSON.stringify(s));
  },
};

export const actions = {
  clear(store) {
    store.commit('set', {
      data: undefined,
      token: undefined,
    });
    localStorage.removeItem(lsKey);
  },
  async get_user(store) {
    const lsState = localStorage.getItem(lsKey);
    if (!lsState) return store.state;

    const s = JSON.parse(lsState);
    if (!s.data || !s.token) return store.state;

    store.commit('set', s);
    return s;
  },
  async login(store, formData) {
    const lsState = localStorage.getItem(lsKey);
    if (!lsState) return store.state;

    const s = JSON.parse(lsState);
    if (!s.data || !s.token) return store.state;

    const { data } = await $axios.$post('/login', formData);
    store.commit('set', { ...s, data });
    return data;
  },
  async register(store, formData) {
    const lsState = localStorage.getItem(lsKey);
    if (!lsState) return store.state;

    const s = JSON.parse(lsState);
    if (!s.data || !s.token) return store.state;

    const { data } = await $axios.$post('/register', formData);
    store.commit('set', { ...s, data });
    return data;
  },
  async update(store, formData) {
    const lsState = localStorage.getItem(lsKey);
    if (!lsState) return store.state;

    const s = JSON.parse(lsState);
    if (!s.data || !s.token) return store.state;

    const { data } = await $axios.$put('/user/' + s.data.id, formData);
    store.commit('set', { ...s, data });
    return data;
  },
};
