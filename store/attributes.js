const lsKey = 'shopx-b2c-attr';

const findOne = (atrr, id) => atrr.id === id;

export const state = () => ({
  list: []
});

export const mutations = {
  set(state, data) {
    if (data.list) state.list = data.list;
    localStorage.setItem(lsKey, JSON.stringify(state));
  },
  clear(state) {
    state.list = [];
    localStorage.removeItem(lsKey);
  },
  load(state) {
      let data = localStorage.getString(lsKey);
      data = data ? JSON.parse(data) : state;
      measure.mutations.SET_DATA(state, data);
  },
};

export const actions = {
  async fetchAll(store) {
    const lsState = localStorage.getItem(lsKey);
    if (!lsState) return store.state;

    const s = JSON.parse(lsState);
    if (!s.list) return store.state;

    let list = await this.$axios.$get('/attribute');

    store.commit('set', {list});

    return list;
  },
};
export const getters = {
  all: state => { return state.list },
  byId: state => id => { return state.list.filter(atrr => { return atrr.id === id }); }
}
