export const isIn = (array, value) => {
  return array.indexOf(value) !== -1;
}

export const findIn = (array, value) => {
  return array.find(el => el === value);
}

export const removeInArray = (array, value) => {
  let index = array.indexOf(value);
  if (index > -1) {
    array.splice(index, 1);
  }
  return array;
}
