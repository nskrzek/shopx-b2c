export const checkQuantityMap = product => {
  if (product.quantity <= product.quantity_min) {
    product._rowVariant = 'danger';
  }
  return product;
};

export const showOnlyActiveFilter = product => product.active === true;
